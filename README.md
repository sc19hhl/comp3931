# COMP3931 Individual Project
This code repository is the code repository for the COMP3931 Individual Project - Deep Learning for Brain Tumor Classification Problem.

## Installation
```bash
git clone git@gitlab.com:sc19hhl/comp3931.git
```

## Dependencies
Dependencies can be found in the requirements.txt file
```bash
pip install -r requirements.txt
```
- TensorFlow
- Requests
- Zipfile
- OpenCV
- NumPy
- Matplotlib
- Seaborn
- tqdm
- SkLearn

## Usage
### Download Dataset
The dataset from figshare can be downloaded using the download.py script
```bash
python download.py
```

### Extraction of Dataset
The dataset can be extracted into their respective folders using extract.py
```bash
python extract.py
```

### CNN-SVM
The CNN-SVM model can be found in the cnn-svm.ipynb

### Transfer Learning Models
The transfer learning models can be found in the transfer_learning.ipynb

### Ensemble model
The ensemble model is also created in the transfer_learning.ipynb

## Notes
Unfortunately due to the size of the models and the dataset, the dataset and the models with the weights we have in the report is not uploaded to gitlab. 