import requests
import zipfile
import io
import os


def download(path=None):
    """Downloads the brain tumor dataset by Cheng et al. from figshare

    Download folder has to be empty to unzip the data

    :param path: Path to save the downloaded files
    :return: None
    """
    cwd = os.getcwd()
    folder = "brainTumorDataPublic"
    download_folder = os.path.join(cwd, folder)

    # Check if dataset was already downloaded
    try:
        if len(os.listdir(download_folder)) > 0 and not path:
            raise FileExistsError("Download folder is not empty")
    except FileNotFoundError:
        pass

    # Download dataset
    url = "https://figshare.com/ndownloader/articles/1512427/versions/5"
    print(f"Downloading dataset from {url}")
    request = requests.get(url, allow_redirects=True)
    zip_file = zipfile.ZipFile(io.BytesIO(request.content))

    # Saving dataset
    if not path:
        try:
            path = os.path.join(cwd, folder)
            os.mkdir(path)
            print(f"Creating {folder} folder.")
        except FileExistsError:
            print(FileExistsError(f"{folder} already created."))
        path = os.path.join(cwd, folder)
    zip_file.extractall(path)
    print(f"Download completed")


def unzip(dataset_path=None):
    """Unzips the brain tumor dataset and saves the folder in the same directory

    :param dataset_path: Path to brainTumorDataPublic
    :return: None
    """
    # Getting dataset path
    cwd = os.getcwd()
    dataset_folder = "brainTumorDataPublic"
    if not dataset_path:
        dataset_path = os.path.join(cwd, dataset_folder)

    # Unzipping folders
    files = os.listdir(dataset_path)
    zip_files = [_zip for _zip in files if ".zip" in _zip]
    for zip_file in zip_files:
        print(f"Extracting {zip_file}")
        path = os.path.join(dataset_path, zip_file)
        zf = zipfile.ZipFile(path)
        zf.extractall(path[:-4])
        print(f"Unzipped {zip_file}")


if __name__ == "__main__":
    download()
    unzip()
