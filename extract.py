import pymatreader as pmr
import matplotlib.pyplot as plt
import os


def create_image(file):
    """Extracts the dictionary from the file

    :param file: mat file path
    :return: label, image, patient ID
    """
    mat = pmr.read_mat(file)
    image = mat['cjdata']['image']
    label = mat['cjdata']['label']
    pid = mat['cjdata']['PID']
    return label, image, pid


def extract(brain_tumor_data_path=None):
    """Extracts all the images from the dataset and saves it as .png

    Extracts the mat file format brain tumor images and saves it in
    the Data folder.

    :param brain_tumor_data_path: Path to brain tumor dataset
    :return: None
    """
    cwd = os.getcwd()

    # Labels for brain tumor
    labels = {
        1: "meningioma",
        2: "glioma",
        3: "pituitary",
    }

    # Create database folder with the labelled subdirectory
    database = os.path.join(cwd, 'Data')
    try:
        path = os.path.join(database)
        # Create data folder
        os.mkdir(path)
        # Create subdirectory
        for key, value in labels.items():
            os.mkdir(os.path.join(path, value))
        print(f"Creating Data folder.")
    except FileExistsError:
        print(FileExistsError(f"Data folder already created."))

    # Brain tumor dataset file paths
    if not brain_tumor_data_path:
        brain_tumor_data_path = os.path.join(cwd, 'brainTumorDataPublic')
    brain_tumor_data = os.listdir(brain_tumor_data_path)
    brain_tumor_data_folder = [folder for folder in brain_tumor_data if "." not in folder]

    # Create and save image from brain tumor dataset
    for folder in brain_tumor_data_folder:
        for file in os.listdir(os.path.join(brain_tumor_data_path, folder)):
            label, image, pid = create_image(os.path.join(brain_tumor_data_path, folder, file))
            label = labels[label]
            # Save image
            filename = file.split('.')[0]
            path = os.path.join(database, label, f"{filename}.{pid}.png")
            plt.imsave(path, image, cmap='gray')
            print(f"Saved {file} to {path}")


if __name__ == "__main__":
    extract()
